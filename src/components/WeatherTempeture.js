import React from 'react';
import PropTypes from 'prop-types';

const WeatherTempeture = ({temperature,weatherState}) => (
    <div>
        <label>Temperatura: <strong>{temperature}° </strong> {weatherState}</label>
    </div>
)

WeatherTempeture.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string
};

export default WeatherTempeture;