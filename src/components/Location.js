import React from 'react';

/*
const Location = () => (
    <div><h1>Querétaro</h1></div>
)
*/
const Location = ({city, localidad}) => (
    <div><h1>{city} <i>{localidad}</i></h1></div>
);

export default Location;