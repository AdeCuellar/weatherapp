import React from 'react';
import PropTypes from 'prop-types';
import WeatherTempeture from './WeatherTempeture';
import WeatherExtraInfo from './WeatherExtraInfo';

const WatherData = (data) => {
    const {temperature,weatherState,humidity,wind} = data.datos;
    return(
        <div>
            <WeatherTempeture temperature={temperature} weatherState={weatherState} />
            <WeatherExtraInfo humidity={humidity} wind={wind} />
        </div>
    )
}

WatherData.propTypes = {
    data: PropTypes.shape({
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired
    })
};

export default WatherData;