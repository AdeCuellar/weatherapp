import React, {Component} from 'react';
import {Preloader} from 'react-materialize';
import Location from './Location';
import WatherData from './WeatherData';
import transformWeather from './../services/transformWeather';
import { createStore } from 'redux';
const store = createStore(() => {}, window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__());

const location = "Querétaro,mx";
const api_key = "f99bbd9e4959b513e9bd0d7f7356b38d";
const api_weather = 'http://api.openweathermap.org/data/2.5/weather?q='+location+',uk&appid='+api_key;
var cont = 2;
/*
Antes se agregaban harkodeados los datos de data en el dom de  WatherData
const data = {
    temperature: 20,
    weatherState: 'nublado',
    humidity: 12,
    wind: '3m/s'
}
*/
class WeatherLocation extends Component
{
    //contructor
    constructor(){
        console.log('constructor');
        super();
        this.state = {
            city : "",
            data : null
        }
    }
    /*
    getWatherSatate = weather => {
        return 'SUN';
    }
    getData = (weather_data) => {
        const {temp,humidity} = weather_data.main;
        const {speed} = weather_data.wind;
        const {description} = weather_data.weather[0];

        const data = {
            temperature: temp-273,
            weatherState: ''+description+'',
            humidity,
            wind: speed
        }
        return data;
    } 
    */
    
    handleUpdateClick = () => {
        
        fetch(api_weather)
        .then(result => {
            return result.json();
            console.log(result);
        }).then(weather_data => {
            const data = transformWeather(weather_data);
            this.setState({ data });
        });

        //prueba cambio de estado en redux
        cont = cont+1;
        const action = {type:'prueba', value : cont};
        store.dispatch(action);
        
    }
    componentWillMount(){
        //llama la función de update valores click
        this.handleUpdateClick();
        console.log('me ejecuto solo en el inicio: ');
    }
    componentDidMount(){
        console.log('me ejecuto despues del render de willmount, tambien solo una vez');
    }
    componentWillUpdate(){
        console.log('componentWillUpdate');
    }
    componentDidUpdate(){
        console.log('despues del render de will update');
    }
    render = () => {
        console.log('render');
        const {city,data} = this.state;
        return(
            <div>
                <Location city={city} localidad={''}/>
                {data ? <WatherData datos={data}/>: <Preloader size='big'/>}
                <button onClick={this.handleUpdateClick}>Actualizar</button>
            </div>
        )
    }
}
export default WeatherLocation;