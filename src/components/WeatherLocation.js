import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Preloader} from 'react-materialize';
import PropTypes from 'prop-types';
import Location from './Location';
import WatherData from './WeatherData';
import transformWeather from './../services/transformWeather';
import {actionCreator} from './../actions';


const location = "Querétaro,mx";
const api_key = "f99bbd9e4959b513e9bd0d7f7356b38d";
const api_weather = 'http://api.openweathermap.org/data/2.5/weather?q='+location+',uk&appid='+api_key;


var cont = 2;

class WeatherLocation extends Component
{
    //contructor
    constructor(){
        console.log('constructor');
        super();
        this.state = {
            city : "",
            data : null
        }
    }
    
    handleUpdateClick = () => {
        
        fetch(api_weather)
        .then(result => {
            return result.json();
            console.log(result);
        }).then(weather_data => {
            const data = transformWeather(weather_data);
            this.setState({ data });
        });

        //prueba cambio de estado en redux
        cont = cont+1;
        //store.dispatch(actionCreator(cont)); Se hacia así antes de conectar la store a react
        this.props.dispatchActionCreator(cont);
        
    }
    componentWillMount(){
        //llama la función de update valores click
        this.handleUpdateClick();
        console.log('me ejecuto solo en el inicio: ');
    }
    componentDidMount(){
        console.log('me ejecuto despues del render de willmount, tambien solo una vez');
    }
    componentWillUpdate(){
        console.log('componentWillUpdate');
    }
    componentDidUpdate(){
        console.log('despues del render de will update');
    }
    render = () => {
        console.log('render');
        const {city,data} = this.state;
        return(
            <div>
                <Location city={city} localidad={''}/>
                {data ? <WatherData datos={data}/>: <Preloader size='big'/>}
                <a class="waves-effect waves-light btn-large" onClick={this.handleUpdateClick}>Actualizar</a>
            </div>
        )
    }
}

//Para declarar la función como una función externa
WeatherLocation.propTypes = {
    actionCreator: PropTypes.func.isRequired,
}

const mapDispatchToPropsActions = dispatch => ({
    dispatchActionCreator: value => dispatch(actionCreator(value))
});
const componentConnected = connect(null,mapDispatchToPropsActions)(WeatherLocation);

export default componentConnected;