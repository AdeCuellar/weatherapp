/*////Creando un store simple desde aquí
import { createStore } from 'redux';

const initialState = {};
const reducer = ( state , action ) => {
    return state;
}
export const store = 
    createStore(reducer, initialState,
        window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()
    );
*/
import { createStore } from 'redux';
import {contador} from './../reducers/contador';
const initialState = {};

export const store = 
    createStore(contador, initialState,
        window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()
    );
